import numpy as np
import math
import pandas as pd
import io
import os
from openpyxl.workbook import Workbook
import matplotlib.pyplot  as plt
class Persona():
    def __init__(self,info,df):
        self.info = info
        self.df = df
        #estructura de info {"edad":0,"nombre":"","ocupacion":"","tels":"","mail":"","nickname":"","ciudad":"","pais":""}
    def GetNums(self,lim):
        if lim > len(self.info["tels"]) or lim < 0:
            lim = len(self.info["tels"])
        return self.info["tels"][:lim]
    def GetMails(self,lim):
        if lim > len(self.info["mails"]) or lim < 0:
            lim = len(self.info["mails"]) #corregir
            #print("no existe esa cantidad de datos\nValor por defecto: ",len(self.info["mails"])) #informar
            #a = lim #ajustar
        return self.info["mails"][:lim]
    def ShowInfo(self,numT,numM):
        print("Nombre: ",self.info['name'])
        print("Edad: ",self.info['Edad'])
        print("Ocupacion: ",self.info['job'])
        print("Telefonos: ",self.GetNums(numT))
        print('Correos',self.GetMails(numM))
    def modify(self,Dato):
        #lo gonorrea que es no tener switch :/
        #Transcribir key
        if Dato == 'Ocupacion':
            Dato = 'job'
            reemplazar = self.info[Dato]
        elif Dato == 'Tel#1' or Dato == 'Tel#2':
            Dato = 'tels'
            if Dato == 'Tel#1':
                reemplazar = self.info[Dato][0]
            else:
                reemplazar = self.info[Dato][1]
        elif Dato == 'Mail#1' or Dato == 'Mail#2':
            Dato = 'mails'
            if Dato == 'Mail#1':
                reemplazar = self.info[Dato][0]
            else:
                reemplazar = self.info[Dato][1]
        elif Dato == 'Nombre completo':
            Dato = 'name'
            reemplazar = self.info[Dato]
        else:
            print("Ese dato no existe ;)")
        print("el Dato: ",reemplazar)
        aux = input("se va a reemplazar por: ")
        Ndf = self.df.replace(to_replace = reemplazar,value=aux)
        self.info[Dato] = aux #para mostrar el cambio en memoria
        self.df = Ndf
        self.df.to_excel("Datos.ods")#para escribir el cambio en el ecxel
        print('dato reemplazado.')
    def count(self,colum,Item):
        elem = self.df.get(colum)
        cont = 0
        for i in range(len(elem.to_list())):
            if Item == elem[i]:
                cont += 1
        return cont
    def delete(self,Dato):
        #lo gonorrea que es no tener switch :/
        #Transcribir key
        if Dato == 'Ocupacion':
            Dato = 'job'
            reemplazar = self.info[Dato]
        elif Dato == 'Tel#1' or Dato == 'Tel#2':
            Dato = 'tels'
            if Dato == 'Tel#1':
                reemplazar = self.info[Dato][0]
            else:
                reemplazar = self.info[Dato][1]
        elif Dato == 'Mail#1' or Dato == 'Mail#2':
            Dato = 'mails'
            if Dato == 'Mail#1':
                reemplazar = self.info[Dato][0]
            else:
                reemplazar = self.info[Dato][1]
        elif Dato == 'Nombre completo':
            Dato = 'name'
            reemplazar = self.info[Dato]
        else:
            print("Ese dato no existe ;)")
        #print("el Dato: ",reemplazar)
        aux = ' '#input("se va a reemplazar por: ")
        Ndf = self.df.replace(to_replace = reemplazar,value=aux)
        self.info[Dato] = aux
        self.df = Ndf
        self.df.to_excel("Datos.ods")
        print('dato eliminado.')
    def Plot(self,Dato):
        x = self.df.get(Dato)
        lx = list(dict.fromkeys(x))#teamopython
        ly = []
        #elem = self.df.get(colum)
        for i in lx:
            if i == 'nan':
                y = 0
            y = self.count(Dato,i)
            #print()
            ly.append(y)
        ts = pd.Series(ly,lx)
        ax = ts.plot.bar(x=Dato)
        ax.set_xlabel(Dato)
        ax.set_ylabel('Cantidad')
        titulo = 'Grafica cantidad de ' + Dato +'s'
        ax.set_title(titulo)
        plt.show()
def cargar():
    a = pd.read_excel("Datos.ods", engine="odf")
    index = 0 #persona #0
    Personas = []
    while (index < len(a.get('Edad').to_list())):
        E = a.get('Edad').to_list()[index]
        N = a.get('Nombre completo').to_list()[index]
        O = a.get('Ocupacion').to_list()[index]
        T = [a.get('Tel#1').to_list()[index],a.get('Tel#2').to_list()[index]]
        M = [a.get('Mail#1').to_list()[index],a.get('Mail#2').to_list()[index]]
        n = a.get('Apodo').to_list()[index]
        C = a.get('Ciudad').to_list()[index]
        P = a.get('Pais').to_list()[index]
        p = Persona(dict(Edad=E,name=N,job=O,tels=T,mails=M,Nickname=n,Ciudad=C,Pais=P),a)
        Personas.append(p)
        index += 1
    return Personas
def filter(Datos,colum,Item):
    elem = Datos[0].df.get(colum)#cualquiera
    for i in range(len(elem.to_list())):
        if Item == elem[i]:
            Datos[i].ShowInfo(1,1)
def buscar(Datos,colum,Item):
    elem = Datos[0].df.get(colum)#cualquiera
    for i in range(len(elem.to_list())):
        if Item == elem[i]:
            return Datos[i]
def opciones():
    print('1.Numero/s')
    print('2.Correo/s')
    print('3.Filtros Agenda')
    print('4.Modificar Dato de la Agenda')
    print('5.Eliminar dato de la Agenda')
    print('6.Graficar')
    print('7.Salir.')
    op = int(input('Opcion --> '))
    return op
def menu(Datos,op):
    if op == 1:
        #filter(Contactos,'Pais','Colombia')
        name = input('Apodo de la persona: ')
        num = int(input('cantidad a mostar: '))
        p = buscar(Datos,'Apodo',name)
        print(p.GetNums(num))
        os.system ("clear")
        a = opciones()
        menu()
    if op == 2:
        #filter(Contactos,'Pais','Colombia')
        name = input('Apodo de la persona: ')
        num = int(input('cantidad a mostar: '))
        p = buscar(Datos,'Apodo',name)
        print(p.GetMails(num))
        os.system ("clear")
        a = opciones()
        menu()
    if op == 3:
        colum = input('Categoria: ')
        item = input('Elemento a buscar: ')
        if item.isdigit() or item.isnumeric():
            int(item)
        filter(Datos,colum,item)
        os.system ("clear")
        a = opciones()
        menu()
    if op == 4:
        name = input('Apodo de la persona: ')
        p = buscar(Datos,'Apodo',name)
        new = input('categoria a Modificar: ')
        p.modify(new)
        os.system ("clear")
        a = opciones()
        menu()
        #Personas[-1].modify('Ocupacion') #modificar
    if op == 5:
        name = input('Apodo de la persona: ')
        p = buscar(Datos,'Apodo',name)
        new = input('categoria a eliminar: ')
        p.delete(new)
        os.system ("clear")
        a = opciones()
        menu()
    if op == 6:
        cat = input('Categoria a Graficar: ')
        Datos[0].Plot(cat)
        os.system ("clear")
        a = opciones()
        menu()
    if op == 7:
        print("salir...")
    else:
        print('Opcion invalida!')
if __name__ == "__main__":
    Contactos = cargar()
    op = opciones()
    menu(Contactos,op)
